package il.co.procyonapps.cointap.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.cointap.R
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.launch

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider
            .AndroidViewModelFactory
            .getInstance(requireActivity().application)
            .create(MainViewModel::class.java)
    }

    private val coinAdapter by lazy { CoinAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvCoinList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = coinAdapter

        }

        viewModel.coinList.observe(viewLifecycleOwner, Observer<List<CoinListItem>> {
            coinAdapter.submitList(it)
        }
        )
    }

}
