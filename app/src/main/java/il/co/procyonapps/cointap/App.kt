package il.co.procyonapps.cointap

import android.app.Application
import il.co.procyonapps.cointap.data.networkschama.CryptoApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App : Application() {
    private val retrofit by lazy {

        val client = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->

            val orig = chain.request()
            val req = orig.newBuilder()
                .header("X-CMC_PRO_API_KEY", "da52b536-df76-4c06-851f-0352f6a4810c")
                .build()

            chain.proceed(req)
        })
            .build()


        Retrofit.Builder()
            .baseUrl("https://pro-api.coinmarketcap.com/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    val api by lazy { retrofit.create(CryptoApi::class.java) }
}