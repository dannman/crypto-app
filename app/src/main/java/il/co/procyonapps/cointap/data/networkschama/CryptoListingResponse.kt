package il.co.procyonapps.cointap.data.networkschama


import com.google.gson.annotations.SerializedName

data class CryptoListingResponse(
    @SerializedName("data") val coins: List<Coin> = listOf(),
    @SerializedName("status") val status: Status = Status()
) {
    data class Coin(
        @SerializedName("circulating_supply") val circulatingSupply: Double = 0.0,
        @SerializedName("cmc_rank") val cmcRank: Int = 0,
        @SerializedName("date_added") val dateAdded: String = "",
        @SerializedName("id") val id: Int = 0,
        @SerializedName("last_updated") val lastUpdated: String = "",
        @SerializedName("max_supply") val maxSupply: Double = 0.0,
        @SerializedName("name") val name: String = "",
        @SerializedName("num_market_pairs") val numMarketPairs: Int = 0,
        @SerializedName("platform") val platform: Any = Any(),
        @SerializedName("quote") val quote: Quote = Quote(),
        @SerializedName("slug") val slug: String = "",
        @SerializedName("symbol") val symbol: String = "",
        @SerializedName("tags") val tags: List<String> = listOf(),
        @SerializedName("total_supply") val totalSupply: Double = 0.0
    ) {
        data class Quote(
            @SerializedName("USD") val uSD: USD = USD()
        ) {
            data class USD(
                @SerializedName("last_updated") val lastUpdated: String = "",
                @SerializedName("market_cap") val marketCap: Double = 0.0,
                @SerializedName("percent_change_1h") val percentChange1h: Double = 0.0,
                @SerializedName("percent_change_24h") val percentChange24h: Double = 0.0,
                @SerializedName("percent_change_7d") val percentChange7d: Double = 0.0,
                @SerializedName("price") val price: Double = 0.0,
                @SerializedName("volume_24h") val volume24h: Double = 0.0
            )
        }
    }

    data class Status(
        @SerializedName("credit_count") val creditCount: Double = 0.0,
        @SerializedName("elapsed") val elapsed: Int = 0,
        @SerializedName("error_code") val errorCode: Int = 0,
        @SerializedName("error_message") val errorMessage: Any = Any(),
        @SerializedName("notice") val notice: Any = Any(),
        @SerializedName("timestamp") val timestamp: String = ""
    )
}