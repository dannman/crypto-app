package il.co.procyonapps.cointap.data.networkschama

import retrofit2.http.GET

interface CryptoApi {

    @GET("cryptocurrency/listings/latest")
    suspend fun getCryptoStateListing(): CryptoListingResponse
}