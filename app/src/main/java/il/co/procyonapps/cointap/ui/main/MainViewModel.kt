package il.co.procyonapps.cointap.ui.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import il.co.procyonapps.cointap.App
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.withContext

class MainViewModel (val app: Application): AndroidViewModel(app) {

    var oldList = emptyMap<String, CoinListItem>()
    val ticker = ticker( 2 * 60 * 1000, 0)

    val coinList: LiveData<List<CoinListItem>> = liveData(Dispatchers.IO) {
        for(tick in ticker) {
            val rawList = (app as App).api.getCryptoStateListing()

            Log.d("MainViewModel", "fetch data")
            val output = rawList.coins.map {
                CoinListItem(it.name, it.quote.uSD.price)
            }
            val compared = compareToOldList(output)

            emit(compared)
        }

    }


    private suspend fun compareToOldList(newList: List<CoinListItem>): List<CoinListItem> = withContext(Dispatchers.Default){
        newList.forEach {newItem->
            val oldItem = oldList[newItem.coinName]

            oldItem?.let {oi ->
                val status = when{
                    oi.dollarVal < newItem.dollarVal -> CoinListItem.Status.INCREASED
                    oi.dollarVal > newItem.dollarVal -> CoinListItem.Status.DECREASED
                    else -> CoinListItem.Status.UNCHANGED
                }
                newItem.status = status
            }
        }

        oldList = newList.map{it.coinName to it}.toMap()

        newList
    }


}
