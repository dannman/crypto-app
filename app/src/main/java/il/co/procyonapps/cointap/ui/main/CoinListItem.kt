package il.co.procyonapps.cointap.ui.main

data class CoinListItem(val coinName: String, val dollarVal : Double, var status: Status = Status.UNCHANGED){
    enum class Status{UNCHANGED, INCREASED, DECREASED}
}