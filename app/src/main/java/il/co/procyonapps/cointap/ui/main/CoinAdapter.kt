package il.co.procyonapps.cointap.ui.main

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import il.co.procyonapps.cointap.R
import kotlinx.android.synthetic.main.list_cell.view.*
import java.text.NumberFormat

class CoinAdapter : ListAdapter<CoinListItem, CoinAdapter.CoinViewHolder>(CoinDiffer()) {

    class CoinDiffer : DiffUtil.ItemCallback<CoinListItem>() {
        override fun areItemsTheSame(oldItem: CoinListItem, newItem: CoinListItem): Boolean {
            return oldItem.coinName == newItem.coinName
        }

        override fun areContentsTheSame(oldItem: CoinListItem, newItem: CoinListItem): Boolean {
            return oldItem.coinName == newItem.coinName && oldItem.dollarVal == newItem.dollarVal && oldItem.status == newItem.status
        }
    }

    class CoinViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(coinListItem: CoinListItem) {

            view.tvCoinName.text = coinListItem.coinName
            view.tvValue.text = NumberFormat.getCurrencyInstance().format(coinListItem.dollarVal)
            val (color, drawable) = when(coinListItem.status){
                CoinListItem.Status.UNCHANGED -> Pair(Color.BLACK, null)
                CoinListItem.Status.DECREASED -> Pair(Color.RED, ContextCompat.getDrawable(view.context, R.drawable.ic_arrow_down_24dp))
                CoinListItem.Status.INCREASED -> Pair(Color.GREEN, ContextCompat.getDrawable(view.context, R.drawable.ic_arrow_up_24dp))
            }
            view.tvValue.setTextColor(color)
            view.tvValue.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinViewHolder {
        return CoinViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_cell, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CoinViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}